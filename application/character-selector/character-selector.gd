extends Node2D

func _ready():
	
	var user_directory = DirAccess.open("user://")
	user_directory.list_dir_begin()
	var user_file_name = user_directory.get_next()
	while user_file_name != "":
		if user_directory.current_is_dir() and user_file_name == "characters":
			break
		user_file_name = user_directory.get_next()
	if user_file_name == "":
		DirAccess.make_dir_absolute("user://characters/")
	
	var character_directory = DirAccess.open("user://characters/")
	character_directory.list_dir_begin()
	var character_file_name = character_directory.get_next()
	var character_button_script = preload("res://application/character-selector/character-button.gd")
	var offset_x = 1
	var offset_y = 0
	while character_file_name != "":
		var character_button = Button.new()
		character_button.position.x += (offset_x * 300) + 100
		character_button.position.y = (offset_y * 300) + 100
		var character_icon = Image.new()
		character_icon.load("user://characters/" + character_file_name + "/icon.png")
		character_button.icon = ImageTexture.create_from_image(character_icon)
		character_button.expand_icon = true
		character_button.size.x = 200
		character_button.size.y = 200
		character_button.name = character_file_name
		character_button.set_script(character_button_script)
		add_child.call_deferred(character_button)
		offset_x += 1
		if offset_x > 4:
			offset_y += 1
			offset_x = 0
		character_file_name = character_directory.get_next()
