extends TextEdit

var characters = []
var letter_checker = RegEx.new()

func _ready():
	
	#Check for character folder and if its not there, create it.
	var user_directory = DirAccess.open("user://")
	user_directory.list_dir_begin()
	var user_file_name = user_directory.get_next()
	while user_file_name != "":
		if user_directory.current_is_dir() and user_file_name == "characters":
			break
		user_file_name = user_directory.get_next()
	if user_file_name == "":
		DirAccess.make_dir_absolute("user://characters/")

	#Get a listing of all characters for later checking
	var character_directory = DirAccess.open("user://characters/")
	character_directory.list_dir_begin()
	var character_file_name = character_directory.get_next()
	while character_file_name != "":
		characters.append(character_file_name)
		character_file_name = character_directory.get_next()

	letter_checker.compile("[^A-Za-z0-9]")


func _process(_delta):
	var label = get_parent()

	if self.text == "":
		label.text = "Name (Can't be empty):"
		return

	if self.text in characters:
		label.text = "Name (Can't be existing character)"
		return

	if letter_checker.search(self.text):
		label.text = "Name (Must be numbers and letters only)"
		return

	label.text = "Name"
