extends OptionButton

var theme_ids = []

func _ready():
	var theme_directory = DirAccess.open("res://data/themes")
	theme_directory.list_dir_begin()
	var theme_file_name = theme_directory.get_next()
	while theme_file_name != "":
		var theme_data_raw = FileAccess.open("res://data/themes/" + theme_file_name + "/meta.json", FileAccess.READ)
		var theme_data = JSON.parse_string(theme_data_raw.get_as_text())
		self.add_item(theme_data["name"])
		theme_ids.append(theme_file_name)
		theme_file_name = theme_directory.get_next()

func get_theme_id(index):
	return theme_ids[index]
