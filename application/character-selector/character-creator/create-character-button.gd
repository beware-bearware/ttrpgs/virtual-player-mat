extends Button

func _process(_delta):
	#check that the user has filled out all required fields of character making
	var name_status = get_node("../name-label").text 

	if name_status != "Name":
		self.disabled = true
		return

	var icon_status = get_node("../icon-label/icon-button").icon

	if icon_status == null:
		self.disabled = true
		return

	self.disabled = false

func _pressed():
	var character_name = get_node("../name-label/name-input").text
	var character_icon = get_node("../icon-label/icon-button")
	DirAccess.make_dir_absolute("user://characters/" + character_name)
	var character_image = character_icon.icon.get_image()
	character_image.save_png("user://characters/" + character_name + "/icon.png")
	var character_data = {}
	var character_theme_node = get_node("../theme-selector-label/theme-selector")
	character_data["theme"] = character_theme_node.get_theme_id(character_theme_node.selected)
	var character_meta_file = FileAccess.open("user://characters/" + character_name + "/meta.json", FileAccess.WRITE)
	var json_string = JSON.stringify(character_data)
	character_meta_file.store_string(json_string)
	
	global.selected_character = self.name
	get_tree().change_scene_to_file("res://application/mat/mat.tscn")
