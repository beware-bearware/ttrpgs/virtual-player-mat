extends FileDialog

func _ready():
	self.current_dir = OS.get_system_dir(OS.SYSTEM_DIR_PICTURES)

func _on_file_selected(_path):
	var icon_button = get_node("../icon-button")
	var image = Image.new()
	image.load(current_path)
	icon_button.icon = ImageTexture.create_from_image(image)
	icon_button.text = ""
