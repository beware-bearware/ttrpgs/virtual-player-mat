extends Node

func _ready():
	pass

func create_card(theme, character):
	var card_base = Control.new()

	#Creating the card background the other nodes will show in front off.
	#Dimensions, in pixels, will be scaled, but use a base of 480x672.
	var scale_x = 480
	var scale_y = 672
	var card_texture = Sprite2D.new()
	var card_image = Image.load_from_file("res://data/themes/" + theme + "/cards/character/design.png")
	card_texture.texture = ImageTexture.create_from_image(card_image)
	card_texture.scale.x = scale_x / card_image.get_size().x
	card_texture.scale.y = scale_y / card_image.get_size().y
	card_texture.centered = false
	card_texture.name = "texture"
	card_base.add_child(card_texture)

	#Get and position the aspects from the layout.json.
	#Content from layout.json only includes what is shown below
	#There is no adding or removing fields on the card without changing this code
	var card_data_raw = FileAccess.open("res://data/themes/" + theme + "/cards/character/layout.json", FileAccess.READ)
	var card_data = JSON.parse_string(card_data_raw.get_as_text())
	
	for card_data_key in card_data.keys():
		if card_data_key != "icon":
			var current_card_data = card_data[card_data_key]
			var text_field = LineEdit.new()
			text_field.position.x = current_card_data["pos_x"]
			text_field.position.y = current_card_data["pos_y"]
			text_field.size.x = current_card_data["size_x"]
			text_field.size.y = current_card_data["size_y"]
			text_field.name = card_data_key
			var font = load("res://data/themes/" + theme + "//cards/character/" + current_card_data["font"])
			text_field.add_theme_font_override("font", font)
			text_field.add_theme_font_size_override("font_size", 12)
			var text_field_script = load("res://application/mat/cards/element-scripts/line-edit.gd")
			text_field.set_script(text_field_script)
			card_base.add_child(text_field)

	var card_icon = Sprite2D.new()
	var card_icon_image = Image.load_from_file("user://characters/" + global.selected_character + "/icon.png")
	card_icon.texture = ImageTexture.create_from_image(card_icon_image)
	card_icon.position.x = card_data["icon"]["pos_x"]
	card_icon.position.y = card_data["icon"]["pos_y"]
	card_icon.scale.x = card_data["icon"]["size_x"] / card_icon_image.get_size().x
	card_icon.scale.y = card_data["icon"]["size_y"] / card_icon_image.get_size().y
	card_icon.centered = false
	card_icon.name = "icon"
	card_base.add_child(card_icon)

	card_base.name = "character_card"
	return card_base
