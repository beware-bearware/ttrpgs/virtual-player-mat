extends LineEdit

var previous_revision = ""


func _ready():
	pass

func _process(_delta):

	#Only contenue if the text in the box has changed to save users resources.
	if self.text == previous_revision:
		return

	var margins = 8
	var text_characters_height = self.get_theme_font("font").get_height(100) / 100
	var max_height = (self.size.y / text_characters_height) - margins
	var max_width = self.size.x - margins

	var font_size = max_height
	var letter_width = self.get_theme_font("font").get_string_size(self.text,0,-1,font_size).x

	#refocus the start of the line edit to prevent letters from leaving the box after being edited
	var old_caret = self.caret_column
	self.caret_column = 0

	#resizing the font if the end of the box is hit to fit everything the user wants
	while max_width < letter_width:
		font_size -= 1
		letter_width = self.get_theme_font("font").get_string_size(self.text,0,-1,font_size).x

	self.caret_column = old_caret

	previous_revision = self.text

	self.add_theme_font_size_override("font_size", font_size)
