extends Node2D

func _ready():
	var character_data_raw = FileAccess.open("user://characters/" + global.selected_character + "/meta.json", FileAccess.READ)
	var character_data = JSON.parse_string(character_data_raw.get_as_text())
	var character_theme = character_data["theme"]

	var character_card = load("res://application/mat/cards/character.gd").new()
	add_child(character_card.create_card(character_theme, global.selected_character))
